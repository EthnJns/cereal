#include<regex>

#include<iostream>

using namespace std;

void printResult(string,bool);

int main(){

    //Base case: int x;
    regex baseCase("[\\s\ta-zA-Z<>]+[a-zA-Z1-9]+;$");
    string bc0 = "int x;";
    string bc1 = "  int x1;";
    string bc2 = "string s, s1;";
    string bc3 = "vector<int> v;";

    printResult(bc0, regex_match(bc0, baseCase));
    printResult(bc1, regex_match(bc1, baseCase));
    printResult(bc2, regex_match(bc2, baseCase));
    printResult(bc3, regex_match(bc3, baseCase));


    //Case: int x; double y; string s; etc.
    regex multiLine("([\\s\ta-zA-Z<>]+[a-zA-Z1-9]+;){2,}");
    string ml0 = "int d1; double d2; myObj obj;";
    string ml1 = "int x;";

    printResult(ml0, regex_match(ml0, multiLine));
    printResult(ml1, regex_match(ml1, multiLine));

    //Case: int x, y, z;
    regex commas("[\\s\ta-zA-Z]+[\\s\ta-zA-Z1-9]+[,\\s\ta-zA-Z1-9]+;$");
    string c0 = "int x, y, z;";
    string c1 = "string s, x;";

    printResult(c0, regex_match(c0, commas));
    printResult(c1, regex_match(c1, commas));

    regex classReg("[\\s\t]*(class)[\\s\ta-zA-Z]+[\n{]*");
    string cR1 = " class myClass{";
    string cR2 = " class myClass   \n{";
    printResult(cR1, regex_match(cR1, classReg));
    printResult(cR2, regex_match(cR2, classReg));

    return 0;
}

void printResult(string exp, bool val){
    if(val){
        cout<<exp<<" matches the regular expression."<<endl;
    }else{
        cout<<exp<<" does not match the regular expression."<<endl;
    }
}