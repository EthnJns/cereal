#include<iostream>
#include<vector>
using namespace std;

int main(){
    int j = 30;
    int p = 10;
    double l = 10.234;
    int* jPtr = &j;
    int* pPtr = &p;
    double* lPtr = &l;
    void* vPtr = &l;
    string s = "test";
    void* vSPtr = &s;
    vector<int> v;
    cout<<j - p<<endl;
    cout<<jPtr<<endl;
    cout<<pPtr<<endl;
    cout<<lPtr<<endl;
    cout<<jPtr-pPtr<<endl;
    cout<<jPtr-(int*)vPtr<<endl;
    cout<<(int*)vPtr-jPtr<<endl;
    cout<<(int*)lPtr - (int*)vSPtr<<endl;

    return 0;
}