#include <iostream>

using namespace std;

class c{
private:
    string l;
    int x;
    double y;
    string s;
    string u;
    //void* locX; void* locY; void* locS;

public:
    void* locX; void* locY; void* locS;
    int distX;
    int distY;
    int distS;
    c(int x, double y, string s){
        cout<<&this->x<<endl;
        this->x = x; this->y = y; this->s = s;
        this->locX=&this->x; this->locY=&this->y; this->locS=&this->s;
    }
    void printDistFromStart(void *v){
        this->distX = (int*)locX - (int*)v;
        this->distY = ((int*)locY - (int*)v);
        this->distS = ((int*)locS - (int*)v);
        cout<<"Distance x: "<<distX<<endl;
        cout<<"Distance y: "<<distY<<endl;
        cout<<"Distance s: "<<distS<<endl;
    }
    int testFunc(int j){
        int p=10; string q;
        return p++;
    }
    
};

int main(){
    c obj(1,2.3,"test");
    void* cv = &obj;
    obj.printDistFromStart(cv);
    cout<<cv<<endl;
    cout<<*(int*)obj.locX<<endl;
    int getX;
    double getY;
    string getS;
    cout<<(int*)((int*)(cv)+(obj.distX))<<endl;
    getX = *(int*)((int*)(cv)+(obj.distX));
    getY = *(double*)((int*)(cv)+(obj.distY));
    getS = *(string*)((int*)(cv)+(obj.distS));
    cout<<getX<<endl;
    cout<<getY<<endl;
    cout<<getS<<endl;
}